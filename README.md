
## Name
Alta3research mastering-ansible certificate exercise.

## Description
This Playbook reconfigures the Backup Config on an NSX-T Manager by
- [ ]  Prompting the user for input, FQDN of the NSX-T Manager, FQDN of the Backup Server, credentials, etc... 
- [ ] getting the SSL Certificate from the Backup Destination
- [ ] applying the required config to the NSX-T Manager.

## Prerequisites
Using Ansible-for-nsxt requires the following packages to be installated. Installation steps differ based on the platform (Mac/iOS, Ubuntu, Debian, CentOS, RHEL etc). Please follow the links below to pick the right platform.

- [ ] Ansible >= 2.9.x Ansible Installation Documentation
- [ ] Python3 == 3.6.13 Python Documentation
- [ ] pip >= 9.x Python Installation PIP installation
- [ ] PyVmOmi - Python library for vCenter api. Installation via pip: pyVmomi installation

## Installation
ansible-for-nsxt modules are distributed as Ansible Galaxy collection. Please use the following command to install it

    ansible-galaxy collection install git+https://github.com/vmware/ansible-for-nsxt

Specify latest supported release branch

    ansible-galaxy collection install git+https://github.com/vmware/ansible-for-nsxt.git,v3.2.0

## Usage
Once installed, the modules can be directly run with ansible-playbook. For example, you can run:

    ansible-playbook --ask-vault-pass  config-backup.yml

For the purpose of this exercise, Account details have been set incorrectly.

    ansible-vault password == password
    

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/piercjs/alta3research-mastering-ansible-cert.git
git branch -M main
git push -uf origin main
```

## Authors and acknowledgment
Thanks to the the ansible-for-nsxt project team.


